﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace TestAppFindFiles
{
    class FileSearcher
    {
        #region Fields

        private BackgroundWorker backgroundWorker;
        private ManualResetEvent pause = new ManualResetEvent(true);
        private IProgress<string> currentFileProgress;
        private IProgress<string> fileFoundProgress;

        #endregion

        #region Events

        /// <summary>
        /// Событие возникает, когда начинается поиск в файле.
        /// </summary>
        public event Action<string> CurrentFileChanged;

        /// <summary>
        /// Событие возникает, когда в текущем файле была найдена искомая строка.
        /// </summary>
        public event Action<string> FileFound;

        /// <summary>
        /// Событие возникает, когда поиск завершается.
        /// </summary>
        public event Action Complete;

        #endregion

        #region Public Properties

        public string DirectoryPath { get; set; } = "";

        public string Pattern { get; set; } = "";

        public string Text { get; set; } = "";

        public int TotalFiles { get; private set; } = 0;

        public string CurrentFile { get; private set; } = "";

        #endregion

        public FileSearcher()
        {
            currentFileProgress = new Progress<string>(file =>
            {
                CurrentFile = file;
                TotalFiles++;
                CurrentFileChanged?.Invoke(file);
            });
            fileFoundProgress = new Progress<string>(file => FileFound?.Invoke(file));
        }

        #region Public Methods

        /// <summary>
        /// Запускает поиск или снимает поиск с паузы.
        /// </summary>
        public void Start()
        {
            if (backgroundWorker == null)
            {
                backgroundWorker = new BackgroundWorker();
                backgroundWorker.WorkerSupportsCancellation = true;
                backgroundWorker.DoWork += BackgroundWorker_DoWork;
                backgroundWorker.RunWorkerCompleted += BackgroundWorker_RunWorkerCompleted;

                backgroundWorker.RunWorkerAsync();
            }
            else
            {
                pause.Set();
            }
        }

        /// <summary>
        /// Ставит поиск на паузу.
        /// </summary>
        public void Pause()
        {
            pause.Reset();
        }

        /// <summary>
        /// Полностью останавливает поиск.
        /// </summary>
        public void Reset()
        {
            pause.Set();
            backgroundWorker.CancelAsync();
        }

        #endregion

        #region Private Methods

        private void BackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            var dir = DirectoryPath;
            var pattern = Pattern;
            var text = Text;

            if (dir == null || pattern == null || text == null)
                return;

            if (pattern == "")
                pattern = "*";

            if (!Directory.Exists(dir))
                return;

            var searchArray = Encoding.UTF8.GetBytes(text);

            foreach (var file in EnumerateFiles(dir, pattern, SearchOption.AllDirectories))
            {
                // Пробует встать на паузу -> Пробует отметиться -> 
                // Рапортует, что приступил к следующему файлу и ищет подстроку
                pause.WaitOne();
                if (backgroundWorker.CancellationPending)
                    return;

                currentFileProgress.Report(file);
                bool lucky = false;
                if (searchArray.Length != 0)
                    try
                    {
                        using (FileStream fs = new FileStream(file, FileMode.Open, FileAccess.Read))
                        {
                            lucky = FastFindArray(fs, searchArray);
                        }
                    }
                    catch
                    {
                        lucky = false;
                    }

                else
                    lucky = true;

                // Так же, пробует встать на паузу -> Пробует отметиться -> 
                // Рапортует о результате текущего поиска
                pause.WaitOne();
                if (backgroundWorker.CancellationPending)
                    return;

                if (lucky)
                    fileFoundProgress.Report(file);
            }
        }

        public static IEnumerable<string> EnumerateFiles(string path, string searchPattern, SearchOption searchOpt)
        {
            try
            {
                var dirFiles = Enumerable.Empty<string>();
                if (searchOpt == SearchOption.AllDirectories)
                {
                    dirFiles = Directory.EnumerateDirectories(path)
                                        .SelectMany(x => EnumerateFiles(x, searchPattern, searchOpt));
                }
                return dirFiles.Concat(Directory.EnumerateFiles(path, searchPattern));
            }
            catch (UnauthorizedAccessException ex)
            {
                return Enumerable.Empty<string>();
            }
        }

        private void BackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            backgroundWorker.Dispose();
            backgroundWorker = null;

            Complete?.Invoke();
            TotalFiles = 0;
            CurrentFile = "";
        }

        /// <summary>
        /// "Тупой" метод поиска массива в потоке. Потребляет много памяти, но на мелких файлах быстрый.
        /// </summary>
        /// <param name="stream">Поток для поиска.</param>
        /// <param name="array">Искомый массив</param>
        /// <returns>В случае обнаружения соответсвующего массива в потоке возвращает true</returns>
        private static bool StupidFindArray(Stream stream, byte[] array)
        {
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);

            return Match(buffer, array);
        }

        /// <summary>
        /// Сложный метод поиска массива в потоке. Одинаково быстро работает с любым размером файла и лишней памяти не потребляет.
        /// </summary>
        /// <param name="stream">Поток для поиска.</param>
        /// <param name="array">Искомый массив</param>
        /// <returns>В случае обнаружения соответсвующего массива в потоке возвращает true</returns>
        private static bool FastFindArray(Stream stream, byte[] array)
        {
            if (array.Length > stream.Length)
                return false;
            else
            {
                int arrayOffset = 0, currentByte;

                while ((currentByte = stream.ReadByte()) != -1)
                {
                    Label:
                    if (currentByte == array[arrayOffset])
                    {
                        if (arrayOffset + 1 == array.Length)
                            return true;

                        arrayOffset++;
                    }
                    else if (arrayOffset != 0)
                    {
                        arrayOffset = Index(array, 1, arrayOffset);
                        goto Label;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Массив ищет себя в себе с учётом смещения start и последнего индекса end.
        /// </summary>
        /// <param name="array">Исходный массив.</param>
        /// <param name="start">Смещение.</param>
        /// <param name="end"></param>
        /// <returns>Возвращает индекс элемента, с которого можно продолжить сравнение.</returns>
        private static int Index(byte[] array, int start, int end)
        {
            if (start >= end)
                return 0;
            else
            {
                int arrayOffset = 0;

                bool fail = false;
                for (int i = start; i < end; i++, arrayOffset++)
                {
                    if (array[i] != array[arrayOffset])
                    {
                        fail = true;
                        break;
                    }
                }

                if (fail)
                    return Index(array, start + 1, end);
                else
                    return arrayOffset;
            }
        }

        /// <summary>
        /// Ищет массив array в массиве src.
        /// </summary>
        /// <param name="src">Массив, в котором производится поиск.</param>
        /// <param name="array">Массив, который ищется.</param>
        /// <returns>В случае удачи возвращает true.</returns>
        private static bool Match(byte[] src, byte[] array)
        {
            bool Match(int offset)
            {
                for (int i = 0; i < array.Length; i++)
                {
                    if (array[i] != src[i + offset])
                        return false;
                }
                return true;
            }

            for (int i = 0; i <= src.Length - array.Length; i++)
            {
                if (Match(i))
                    return true;
            }
            return false;
        }

        #endregion
    }
}