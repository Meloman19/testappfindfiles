﻿using System;
using System.Windows.Forms;

namespace TestAppFindFiles
{
    public interface IMainForm
    {
        /// <summary>
        /// Устанавливает корневой элемент TreeView.
        /// </summary>
        /// <param name="root">Нода TreeView.</param>
        void SetTreeDate(TreeNode root);

        /// <summary>
        /// Устанавливает имя основной кнопки, запускающей поиск.
        /// </summary>
        /// <param name="name">Имя кнопки.</param>
        void SetButtonName(string name);

        /// <summary>
        /// Устанавливает имя текущего файла, в котором осуществляется поиск строки.
        /// </summary>
        /// <param name="file">Путь до файла.</param>
        void SetCurrentFile(string file);

        /// <summary>
        /// Устанавливает общее количество обработанных файлов.
        /// </summary>
        /// <param name="count">Количество обработанных файлов.</param>
        void SetTotalFile(int count);

        /// <summary>
        /// Устанавливает прошедшее с начала поиска время.
        /// </summary>
        /// <param name="time">Время поиска.</param>
        void SetTime(TimeSpan time);

        /// <summary>
        /// Уведомляет о завершении операции поиска.
        /// </summary>
        void Complete();
    }
}