﻿using System.ComponentModel;
using System.IO;

namespace TestAppFindFiles
{
    public class SearchControlPresenter
    {
        ISearchControl searchControl;

        BindingList<string> fileNamePatternsList;
        BindingList<string> directoryPathList;
        BindingList<string> searchTextList;

        public string CurrentNamePattern { get; private set; } = "";
        public string CurrentDirectory { get; private set; } = "";
        public string CurrentSearchText { get; private set; } = "";

        public SearchControlPresenter(ISearchControl searchControl)
        {
            this.searchControl = searchControl;

            fileNamePatternsList = new BindingList<string>(Properties.Settings.Default.NamePatternList);
            directoryPathList = new BindingList<string>(Properties.Settings.Default.DirectoryList);
            searchTextList = new BindingList<string>(Properties.Settings.Default.SearchTextList);

            searchControl.SetPatternData(fileNamePatternsList);
            searchControl.SetDirectoryData(directoryPathList);
            searchControl.SetSearchTextData(searchTextList);
        }

        /// <summary>
        /// Добавляет введённые в поля данные в списки.
        /// </summary>
        public void Add()
        {
            if (!Directory.Exists(searchControl.CurrentDirectory))
                throw new System.Exception("Указанного пути не существует");

            CurrentNamePattern = searchControl.CurrentNamePattern;
            CurrentDirectory = searchControl.CurrentDirectory;
            CurrentSearchText = searchControl.CurrentSearchText;

            if (Insert(fileNamePatternsList, CurrentNamePattern))
                searchControl.SetFirstPattern();
            if (Insert(directoryPathList, CurrentDirectory))
                searchControl.SetFirstDirectory();
            if (Insert(searchTextList, CurrentSearchText))
                searchControl.SetFirstText();
        }

        private bool Insert(BindingList<string> list, string s)
        {
            if (s != "")
            {
                if (list.Contains(s))
                    list.Remove(s);

                list.Insert(0, s);

                while (list.Count > 10)
                    list.RemoveAt(list.Count - 1);

                return true;
            }
            else
                return false;
        }
    }
}