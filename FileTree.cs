﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace TestAppFindFiles
{
    class FileTree
    {
        private string rootDirectory = "";

        /// <summary>
        /// Корень файлового дерева.
        /// </summary>
        public TreeNode Root { get; }

        public FileTree(string rootDir)
        {
            rootDirectory = rootDir.TrimEnd(Path.DirectorySeparatorChar);
            Root = new TreeNode(Path.GetFileName(rootDirectory));
        }

        /// <summary>
        /// Добавляет в дерево новый путь относительно базового.
        /// </summary>
        /// <param name="path">Добавляемый путь к файлу.</param>
        public void Add(string path)
        {
            var rel = RelativePath(path, rootDirectory).Split('\\');
            var node = Add(Root, rel, 0);
            node.Tag = path;
            node.ContextMenu = new ContextMenu();
            node.ContextMenu.Tag = path;
            node.ContextMenu.MenuItems.Add("Открыть файл", treeView_Open);
            node.ContextMenu.MenuItems.Add("Открыть расположение", treeView_ContextMenu);
        }

        private static void treeView_Open(object sender, EventArgs e)
        {
            var path = (sender as MenuItem)?.Parent?.Tag as string;
            if (path == null)
                return;

            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private static void treeView_ContextMenu(object sender, EventArgs e)
        {
            var path = (sender as MenuItem)?.Parent?.Tag as string;
            if (path == null)
                return;

            try
            {
                System.Diagnostics.Process.Start("explorer.exe", "/select," + path);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Рекурсивное создание дерева.
        /// </summary>
        /// <param name="node">Текущая нода.</param>
        /// <param name="path">Массив элементов пути.</param>
        /// <param name="index">Индекс текущего элемента пути.</param>
        /// <returns>Возвращает конечную ноду.</returns>
        private static TreeNode Add(TreeNode node, string[] path, int index)
        {
            if (index < path.Length)
            {
                TreeNode tempNode;

                if (node.Nodes.ContainsKey(path[index]))
                    tempNode = node.Nodes[path[index]];
                else
                    tempNode = node.Nodes.Add(path[index], path[index]);

                return Add(tempNode, path, index + 1);
            }
            else
                return node;
        }

        private static string RelativePath(string absPath, string relTo)
        {
            string[] absDirs = absPath.Split('\\').Where(x => x != "").ToArray();
            string[] relDirs = relTo.Split('\\').Where(x => x != "").ToArray();

            for (int i = 0; i < relDirs.Length; i++)
                if (!relDirs[i].Equals(absDirs[i], StringComparison.CurrentCultureIgnoreCase))
                    throw new Exception("RelativePath");

            var startindex = relDirs.Length;

            return string.Join("\\", absDirs.Skip(startindex));
        }
    }
}