﻿using System;
using System.Windows.Forms;

namespace TestAppFindFiles
{
    public partial class MainForm : Form, IMainForm
    {
        public MainFormPresenter Presenter { get; }

        public MainForm()
        {
            InitializeComponent();

            Presenter = new MainFormPresenter(this, searchControl.Presenter);
        }

        private void buttonStartSearch_Click(object sender, EventArgs e)
        {
            searchControl.Enabled = false;
            buttonCancel.Enabled = true;
            Presenter.StartPause();
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            buttonStartSearch.Enabled = false;
            Presenter.Reset();
        }

        #region IMainForm

        public void SetTreeDate(TreeNode root)
        {
            treeView.Nodes.Clear();
            treeView.Nodes.Add(root);
        }

        public void SetButtonName(string name) => buttonStartSearch.Text = name;

        public void SetCurrentFile(string file)
        {
            toolStripStatusLabelLeft.Text = file;
        }

        public void SetTotalFile(int count)
        {
            toolStripStatusLabelRigth.Text = count.ToString();
        }
        public void Complete()
        {
            buttonCancel.Enabled = false;
            searchControl.Enabled = true;
            buttonStartSearch.Enabled = true;
            buttonStartSearch.Text = "Начать поиск";
        }

        public void SetTime(TimeSpan timeSpan) =>
            toolStripStatusLabelTime.Text =
                timeSpan.Minutes.ToString().PadLeft(2, '0') +
                ":" +
                timeSpan.Seconds.ToString().PadLeft(2, '0');


        #endregion
    }
}