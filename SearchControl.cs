﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace TestAppFindFiles
{
    public partial class SearchControl : UserControl, ISearchControl
    {
        public SearchControlPresenter Presenter { get; }

        public SearchControl()
        {
            InitializeComponent();

            Presenter = new SearchControlPresenter(this);
        }

        #region ISearchControl

        public string CurrentNamePattern => cbNamePattern.Text;

        public string CurrentDirectory => cbDirectoryPath.Text;

        public string CurrentSearchText => cbSearchText.Text;

        public void SetPatternData(BindingList<string> data) => cbNamePattern.DataSource = data;

        public void SetDirectoryData(BindingList<string> data) => cbDirectoryPath.DataSource = data;

        public void SetSearchTextData(BindingList<string> data) => cbSearchText.DataSource = data;

        public void SetFirstPattern() => cbNamePattern.SelectedIndex = 0;

        public void SetFirstDirectory() => cbDirectoryPath.SelectedIndex = 0;

        public void SetFirstText() => cbSearchText.SelectedIndex = 0;

        #endregion

        private void buttonSelectDir_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog folderBrowserDialog = new FolderBrowserDialog();

            if (folderBrowserDialog.ShowDialog() == DialogResult.OK)
            {
                cbDirectoryPath.Text = folderBrowserDialog.SelectedPath;
            }
        }

        private void Combobox_Resize(object sender, EventArgs e)
        {
            var cb = sender as ComboBox;
            if (!cb.Focused)
                cb.SelectionLength = 0;
        }
    }
}