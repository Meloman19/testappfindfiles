﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace TestAppFindFiles
{
    class MyStopwatch
    {
        public event Action Tick;

        private Timer timer = new Timer();
        private Stopwatch stopwatch = new Stopwatch();

        /// <summary>
        /// Создаёт экземпляр класса MyStopwatch с заданным интервалом уведомления.
        /// </summary>
        /// <param name="interval">Интервал уведомления в мс.</param>
        public MyStopwatch(int interval) : base()
        {
            timer.Interval = interval;

            timer.Tick += (s, e) => Tick?.Invoke();

        }

        /// <summary>
        /// Запускает таймер.
        /// </summary>
        public void Start()
        {
            timer.Start();
            stopwatch.Reset();
            stopwatch.Start();
        }

        /// <summary>
        /// Останавливает таймер.
        /// </summary>
        public void Stop()
        {
            timer.Stop();
            stopwatch.Stop();
        }

        /// <summary>
        /// Получает общее затраченное время, измеренное текущим экземпляром.
        /// </summary>
        public TimeSpan Elapsed => stopwatch.Elapsed;
    }
}