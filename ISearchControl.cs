﻿using System.ComponentModel;

namespace TestAppFindFiles
{
    public interface ISearchControl
    {
        /// <summary>
        /// Возвращает текущее значение в поле шаблона.
        /// </summary>
        string CurrentNamePattern { get; }

        /// <summary>
        /// Возвращает текущее значение в поле директории.
        /// </summary>
        string CurrentDirectory { get; }

        /// <summary>
        /// Возвращает текущее значение в поле искомого текста.
        /// </summary>
        string CurrentSearchText { get; }

        /// <summary>
        /// Привязывает список ранее вводимых значений в поле шаблона.
        /// </summary>
        void SetPatternData(BindingList<string> data);

        /// <summary>
        /// Привязывает список ранее вводимых значений в поле директории.
        /// </summary>
        void SetDirectoryData(BindingList<string> data);

        /// <summary>
        /// Привязывает список ранее вводимых значений в поле искомого текста.
        /// </summary>
        void SetSearchTextData(BindingList<string> data);

        // Столкнулся с непонятным поведением: при изменении списка в ComboBox,
        // автоматически выбирается последний. Пришлось сделать такой костыль.
        /// <summary>
        /// Устанавливает первые значения в поля.
        /// </summary>
        void SetFirstPattern();
        void SetFirstDirectory();
        void SetFirstText();
    }
}