﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TestAppFindFiles
{
    public class MainFormPresenter
    {
        private enum State
        {
            Stop,
            Start,
            Pause
        }

        #region Fields

        private IMainForm mainForm;
        private SearchControlPresenter searchControlPresenter;
        private State state = State.Stop;

        private Timer currentFileUpdater = new Timer()
        {
            Interval = 50
        };
        private FileSearcher fileFinder;
        private FileTree fileTree = new FileTree("");
        private MyStopwatch stopwatch = new MyStopwatch(1000);

        #endregion

        public MainFormPresenter(IMainForm mainForm, SearchControlPresenter searchControlPresenter)
        {
            this.searchControlPresenter = searchControlPresenter;
            this.mainForm = mainForm;

            mainForm.SetTreeDate(fileTree.Root);
            stopwatch.Tick += Stopwatch_Tick;
            currentFileUpdater.Tick += CurrentFileUpdater_Tick;

            fileFinder = new FileSearcher();
            fileFinder.Complete += FileFinder_Complete;
            fileFinder.FileFound += FileFinder_FileFound;
        }

        private void CurrentFileUpdater_Tick(object sender, EventArgs e)
        {
            mainForm.SetCurrentFile(fileFinder.CurrentFile);
            mainForm.SetTotalFile(fileFinder.TotalFiles);
        }

        #region Public Methods

        /// <summary>
        /// Запуск/пауза поиска.
        /// </summary>
        public void StartPause()
        {
            switch (state)
            {
                case State.Stop:
                    try
                    {
                        searchControlPresenter.Add();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        FileFinder_Complete();
                        break;
                    }

                    var dir = Path.GetFullPath(searchControlPresenter.CurrentDirectory);

                    fileFinder.DirectoryPath = Path.GetFullPath(dir);
                    fileFinder.Pattern = searchControlPresenter.CurrentNamePattern;
                    fileFinder.Text = searchControlPresenter.CurrentSearchText;

                    fileTree = new FileTree(dir);
                    mainForm.SetTreeDate(fileTree.Root);

                    stopwatch.Start();
                    currentFileUpdater.Start();

                    state = State.Start;
                    fileFinder.Start();
                    mainForm.SetButtonName("Пауза");
                    break;
                case State.Start:
                    state = State.Pause;
                    fileFinder.Pause();
                    mainForm.SetButtonName("Продолжить");
                    break;
                case State.Pause:
                    state = State.Start;
                    fileFinder.Start();
                    mainForm.SetButtonName("Пауза");
                    break;
            }
        }

        /// <summary>
        /// Остановка поиска.
        /// </summary>
        public void Reset()
        {
            fileFinder.Reset();
        }

        #endregion

        #region Private Methods

        private void Stopwatch_Tick()
        {
            mainForm.SetTime(stopwatch.Elapsed);
        }

        private void FileFinder_Complete()
        {
            stopwatch.Stop();
            currentFileUpdater.Stop();
            state = State.Stop;

            mainForm.SetCurrentFile("");
            mainForm.SetTotalFile(fileFinder.TotalFiles);

            mainForm.Complete();
        }

        private void FileFinder_FileFound(string obj)
        {
            fileTree.Add(obj);
        }

        #endregion
    }
}