﻿using System;
using System.Windows.Forms;

namespace TestAppFindFiles
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new MainForm());

            // Сохранение настроек.
            Properties.Settings.Default.Save();
        }
    }
}